<?php
error_reporting(E_ALL);
include 'utilities.php';
include 'markdown.php';

$path = 'assets/markdown/';
$extensions = array('.markdown', '.md');


// get the file name from query
$_CLEAN['GET'] = clean($_GET);
$q = $_CLEAN['GET']['q'];

$filename = $path.$q.$extensions[0];

if (!file_exists($filename)) {
	$filename = $path.$q.$extensions[1];
}

print '<a href="/'.$filename.'">'.$filename."</a>";
if ($q) {
	echo Markdown(file_get_contents($filename));
}
