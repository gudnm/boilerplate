jQuery(document).ready(function($) {
	$('#full-width-slider').royalSlider({
	 	arrowsNav: true,
	 	loop: true,
	 	keyboardNavEnabled: true,
 		controlsInside: false,
	 	imageScaleMode: 'fill',
	 	arrowsNavAutoHide: false,
	 	autoScaleSlider: true, 
	 	autoScaleSliderWidth: 16, 
		autoScaleSliderHeight: 7,
		controlNavigation: 'bullets',
		thumbsFitInViewport: false,
		navigateByClick: false,
		transitionType:'move',
		globalCaption: false,
		autoPlay: {
    			// autoplay options go gere
    			enabled: true,
    			pauseOnHover: true,
    			delay: 4000
    		}
	});
});

