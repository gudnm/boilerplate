<?php
include 'template.php';
$page = new Template('views/pageView.php', array(
    'headerView' => new Template('views/headerView.php', array(
	//replace title
        'title' => 'Page view - Themworks',
    )),
    'linkView' => new Template('views/linkView.php', array(
        'page' => TRUE,
    )),
    'pageContentView' => new Template('views/pageContentView.php', array(
//pass in md file for content
        'content' => 'assets/markdown/1.md',
    )),
    'footerView' => new Template('views/footerView.php', array(
	'page' => TRUE,
    )),
));

$page->render();
