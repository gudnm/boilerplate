<?php
include 'template.php';
$contact = new Template('views/contactView.php', array(
    'headerView' => new Template('views/headerView.php', array(
        'title' => 'Contact Form - Themworks',
    )),
    'linkView' => new Template('views/linkView.php', array(
        'contact' => TRUE,
    )),
    'contactFormView' => new Template('views/contactFormView.php', array()),
    'footerView' => new Template('views/footerView.php', array(
        'form' => TRUE,
    )),
));

$contact->render();
