- Logo should be a 2/3 of current size. 
- We want to see Overall Murals a little bit lower, with murals overlapping completely.
- back ground should be pure white

all three are quick fixes. 

*To Jason: Dude, can we make things to switch on/off like the background? Not on the website itself, but at least easily accesible from the code?*

*How hard is it to make the same for fonts?*

- different options than the little dots in slider? or take dots out completely

dots are kind of industry standard.  

- Add hairline black frame around the slider and make slide view slower
- slider, can we make it taller? if we have to sacrifice some width that's fine

noted.

- let's try moving instagram and twitter to bottom of page and we'll fill in the right of the "recent projects" on homepage with something else (for now let's say an about line and maybe client logos?) - going back to your original suggestion


cool, we'll mock something up.

- Titles - "recent projects", "twitter", "instagram" in bold black, not italicized, make CAPS
- Send screenshots using the different fonts for titles and links (from google web fonts) - oswald, Yanone Kaffeesatz, voltaire, tenor sans, anton, coda, share
- Fonts on all links (titles and content) should be smaller, 2x smaller
- Want to see with only one highlight color - no green, just orange. All links, take out drop shadows on all titles, change green to BLACK except on Locations and Services, sub-titles can remain orange
- Brand / client names can be Orange for now
- Projects page - descriptions, again all fonts should be much smaller, can we move description and name of project up one line
- Content below each project, left justify

noted. I'm guessing Jason will have some suggestions on these. UX is his domain. 

- Previous and Next project buttons - other options? 
- Need to stylize project page and individual project pages bit more
- Videos within each project, is there a better way to separate and stylize where it sits? Also, every video within the project page are all different sizes, can we streamline them?

good point. 

*To Jason: Here's how they look now:*

    <iframe width="560" height="315" src="http://www.youtube.com/embed/RUwoSDVJO88" frameborder="0" allowfullscreen></iframe>

    <iframe src="http://player.vimeo.com/video/39669152?color=ffffff" width="500" height="281"></iframe>

*They're all iframe's and all have width and height. What I'm gonna do is get rid of the inline styling and add a class of, let's say, projectvideo. Then you can just style them, right?*

- On each link and page, the bottom section (not sure what this is called) should have more information... looks too naked (again per original ideas, let's add twitter and instagram here). What else??
- add more icons to social media, flickr and vimeo
phone number should be 718-964-8000

noted.

- Not a fan of how the site looks when viewed vertically on iphone - is there a way to make menu smaller or anything like that?

we'll work on that. 

- Site does not translate on firefox

in my version of Firefox only the tweet list is not right, that is a quick fix. What else do you see that I'm not seeing?

*To Jason: Everything looks fine in firefox except tweet list on home page hiding because of 'overflow: hidden;' in main.css file*

    .tweet_list {
        background-color: #F7F7F7;
        list-style: none outside none;
        margin: 0;
        overflow: hidden;
        padding: 0 0.5em;
    }

*what is the purpose of that overflow: hidden? It seems to work fine in Safari without it.*