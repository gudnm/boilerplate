<!-- BEGIN HEADER -->
<header class="header" id="main">

        <!-- BEGIN NAVIGATION -->
        <nav role="navigation">
             <ul class="site-nav">
                 <li><a class="last<? if ($this->contact) echo ' active'; ?>" href="/contact">Contact</a></li>
             </ul><!-- end of #menu -->
        </nav>
        <!-- END NAVIGATION -->

    <!-- /LOGO -->
        <a href="/home" id="logo"><img src="/assets/image-base/logo.png" alt="Boilerplate Logo"><h1>Boilerplate</h1></a>
    <!-- \LOGO -->

</header>
<!-- END HEADER -->
