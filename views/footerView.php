
<!-- BEGIN FOOTER -->
<footer role="contentinfo">
  <nav class="clearfix">
    <ul class="social">
      <li class="icon"><a class="fc-webicon small twitter" href="https://twitter.com/Themworks"></a></li>
    </ul>
    <address>
      <div><a href="mailto:info@themworks.com">info@themworks.com</a>&nbsp;<a href="tel:+10000000000">(000) 000-0000</a></div>
    </address>
	</nav>

    <div id="footer-bottom">Copyright &copy; <? date_default_timezone_set('America/New_York'); echo date("Y");?> ThemWorks, All rights reserved.</div>
</footer>
<!-- END FOOTER -->
  	

<!-- JavaScript at the bottom for fast page loading -->
<? if ($this->home) {?>
	<!-- conditional embedagram JS file and script  -->
	<script src="/code/js/jquery-embedagram.pack.js"></script>
        <script>
          $(document).ready(function() {
	    $('#slideshow').embedagram({
		    instagram_id:207715195,
			    limit:6,
			    thumb_width:80,
			   wrap_tag: 'span class=test' 
	    });
          });
	</script>

	<!-- conditional tweet! JS file and script  -->
	<script src="/code/js/tweet/jquery.tweet.js"></script>
	<script type='text/javascript'>
    	  jQuery(function($){
           $(".tweet").tweet({
            username: "overallmurals",
            join_text: "auto",
            avatar_size: 42,
            count: 3,
            auto_join_text_default: "", 
            auto_join_text_ed: "",
            auto_join_text_ing: "",
            auto_join_text_reply: "",
            auto_join_text_url: "",
            loading_text: "loading tweets..."
           });
          });
	</script>
<? } ?>
	
<? if ($this->grid) {?> 
	<!-- conditional gridRotator JS files -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script src="/code/js/jquery.transit.min.js"></script>
        <script src="/code/js/jquery.gridrotator.js"></script>
        <script src="/code/js/css3-mediaqueries.js"></script>

        <script> 
          $(function() {
            $( '#ri-grid' ).gridrotator();
          });     
        </script>
<? } ?>

<? if ($this->form) {?> 
<script src='/code/js/formscript.js'></script>
<? } ?>

	<!-- this is for IE to understand HTML5 elements, eventually this will go to a concatenated and minified script -->
	<script>
	  document.createElement('header');
	  document.createElement('footer');
	  document.createElement('section');
	  document.createElement('aside');
	  document.createElement('nav');
  	  document.createElement('article');
	</script>

  	<!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID.
       	mathiasbynens.be/notes/async-analytics-snippet -->
	<script>
    	  var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    	  (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    	  g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    	  s.parentNode.insertBefore(g,s)}(document,'script'));
  	</script>
<!-- Flow Tracking (https://flow.mixpanel.com) -->
<!-- start Mixpanel --><script type="text/javascript">(function(c,a){window.mixpanel=a;var b,d,h,e;b=c.createElement("script");b.type="text/javascript";b.async=!0;b.src=("https:"===c.location.protocol?"https:":"http:")+'//cdn.mxpnl.com/libs/mixpanel-2.1.min.js';d=c.getElementsByTagName("script")[0];d.parentNode.insertBefore(b,d);a._i=[];a.init=function(b,c,f){function d(a,b){var c=b.split(".");2==c.length&&(a=a[c[0]],b=c[1]);a[b]=function(){a.push([b].concat(Array.prototype.slice.call(arguments,0)))}}var g=a;"undefined"!==typeof f?
g=a[f]=[]:f="mixpanel";g.people=g.people||[];h="disable track track_pageview track_links track_forms register register_once unregister identify name_tag set_config people.identify people.set people.increment".split(" ");for(e=0;e<h.length;e++)d(g,h[e]);a._i.push([b,c,f])};a.__SV=1.1})(document,window.mixpanel||[]);
mixpanel.init("2d80391928fe7ba0cf34f001fc1a0ce5");</script><!-- end Mixpanel -->
<!-- Piwik --> 
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://www.swetzoff.com/analytics/" : "http://www.swetzoff.com/analytics/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 2);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://www.swetzoff.com/analytics/piwik.php?idsite=2" style="border:0" alt="" /></p></noscript>
<!-- End Piwik Tracking Code -->
