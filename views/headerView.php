<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Overall Murals website." />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0">
    <title><?= $this->title; ?></title>
    
    <!-- Favorites icon -->
    <link rel="shortcut icon" href="/assets/favicon.ico" />

    <!-- Stylesheets -->
    <link rel="stylesheet" href="/code/css/main.css">
    <!--link rel="stylesheet" href="code/css/print.css" media="print"-->
    <link rel="stylesheet" href="/code/css/holmes.min.css">
    <link href="/code/css/jquery.tweet.css" media="all" rel="stylesheet" type="text/css"/>
    <style type="text/css">@media print {.site-nav {display:none;} .logo {margin-top:20px;}}</style> 
    <!-- Google Fonts -->   
 <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,600,700' rel='stylesheet' type='text/css'> -->
    <!-- Modernizr -->
    <script src="/code/js/modernizr-2.5.3.min.js"></script>

  	<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
    	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  	<script>window.jQuery || document.write('<script src="code/js/jquery.js"><\/script>')</script>
	
<? if ($this->slider) {?>
    	<!-- conditional slider JS files -->
    	<link href="/code/js/royalslider/royalslider.css" rel="stylesheet">
    	<script src="/code/js/royalslider/jquery.royalslider.custom.min.js"></script>
	<script src="/code/js/royalslider/settings.js"></script>
    	<link href="/code/js/royalslider/minimal-white/rs-minimal-white.css" rel="stylesheet">
<? } ?>
</head>
