<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<? $this->headerView->render(); ?>

<body>
	<!-- BEGIN PAGE -->
	<div class="page clearfix">
		<div class="background">
			<? $this->linkView->render(); ?>		
			<? $this->pageContentView->render(); ?>
			<? $this->footerView->render(); ?>
		</div><!-- END BACKGROUND -->
	</div>
	<!-- END PAGE -->
</body>
</html>
