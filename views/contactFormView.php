<div class="title contactTitle">
    <h2>Get In Touch</h2>
</div>

  <!-- CONTENT -->
  <div role="main" class="main-content contact clearfix">
    <!-- Row -->
    <div class="row row-1 clearfix">


        <div id="contact-form">
       
            <?php
			//init variables
			$cf = array();
			$sr = false;
			if(isset($_SESSION['cf_returndata'])){
				$cf = $_SESSION['cf_returndata'];
			 	$sr = true;
			}
            ?>
            <ul id="errors" class="<?php echo ($sr && !$cf['form_ok']) ? 'visible' : ''; ?>">
                <li id="info" class="alert alert-warning">You must have entered something wrong. Please check and hit 'send' again.</li>
                <?php 
				if(isset($cf['errors']) && count($cf['errors']) > 0) :
					foreach($cf['errors'] as $error) :
				?>
                <li class="alert alert-info"><?php echo $error ?></li>
                <?php
					endforeach;
				endif;
				?>
            </ul>
            
            <p id="success" class="<?php echo ($sr && $cf['form_ok']) ? 'visible' : ''; ?> alert alert-success">Thanks for your message. We will get back to you soon.</p>
            <div class="well full">
            <form method="post" action="form-process.php">
                <div class="contactCol">
                <label for="name">Name: <span class="required">*</span></label>
                <input type="text" id="name" name="name" value="<?php echo ($sr && !$cf['form_ok']) ? $cf['posted_form_data']['name'] : '' ?>" placeholder="John Doe" required autofocus />
                
                <label for="email">Email Address: <span class="required">*</span></label>
                <input type="email" id="email" name="email" value="<?php echo ($sr && !$cf['form_ok']) ? $cf['posted_form_data']['email'] : '' ?>" placeholder="johndoe@example.com" required />
                
                <label for="telephone">Telephone: </label>
                <input type="tel" id="telephone" name="telephone" value="<?php echo ($sr && !$cf['form_ok']) ? $cf['posted_form_data']['telephone'] : '' ?>" />
                </div>

            <div class="contactCol omega">                
                <label for="message">Message: <span class="required">*</span></label>
                <textarea id="message" name="message" placeholder="Your message must be greater than 20 charcters" required data-minlength="20"><?php echo ($sr && !$cf['form_ok']) ? $cf['posted_form_data']['message'] : '' ?></textarea>
                
            </div>
            <div class="btnSubmit">
             <span id="loading"></span>
                <input type="submit" value="Send" id="submit-button" class="btn" />
            </div>
            </form>
            <?php unset($_SESSION['cf_returndata']); ?>
            <div class="help-block">
                <p id="req-field-desc"><span class="required">*</span> indicates a required field</p>
            </div>

            </div><!-- end well -->
        </div>

    </div>
</div>
