<?php
include 'template.php';

$path = 'assets/markdown/';

//find all posts
$files = glob($path.'*');

// $posts: $title=>$link
$posts = array();
foreach($files as $file) {
	$path_parts = pathinfo($file);
	$filename = $path_parts['filename'];
	$posts[$file] = '/posts/'.$filename;
}


$args = array(
	'headerView' => new Template('views/headerView.php', array(
		'title' => 'Blog',
	)),
	'linkView' => new Template('views/linkView.php', array(
		'blog' => TRUE,
	)),
	'blogContentView' => new Template('views/blogContentView.php', array(
		'posts' => $posts,
	)),
	'footerView' => new Template('views/footerView.php', array()),
);

$blog = new Template('views/blogView.php', $args);

$blog->render();
