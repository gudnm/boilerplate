# Description 
Crank up responsive websites as quickly as possible. Contains Vagrant file and provisioning script, simple PHP templating engine and example views, .htaccess file for link rewriting.

# Provisioning
There are no requirements to development environment except latest Virtualbox, vagrant and one of Ubuntu base boxes that come with vagrant. Make sure you have port 8080 available and base box is stored under 'base' name.

# Boilerplate
.htaccess is essentially used as a routing mechanism.
Data is separated from presentation with simple PHP templates. File template.php contains templating class, landing pages (page.php, blog.php, contact.php) are templates, views are in views/ directory.
Stylesheets are compiled by compass.
Boilerplate uses modernizr and jquery.
